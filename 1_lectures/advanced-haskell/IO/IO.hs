import Data.Char

cap :: String -> String
cap xs = map toUpper xs

main =
  do
    putStrLn "What's your name?"
    name <- getLine
    putStrLn $ "Hi " ++ (cap name)
    
