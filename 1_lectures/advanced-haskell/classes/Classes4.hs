class Shape s where
  area :: s -> Float

data Rectangle = Rectangle Float Float deriving (Show,Eq) 
data Circle    = Circle Float deriving (Show,Eq) 
                                                   
instance Shape Rectangle where
  area (Rectangle w h) = w * h

instance Shape Circle where
  area (Circle r)      = pi * r * r

totalArea :: Shape s => [s] -> Float
totalArea xs = sum (map area xs)

-- problem: cannot have a list of rectangles and circles
-- because lists in Haskell must be homogeneous

-- for possible solutions
-- see https://wiki.haskell.org/Heterogenous_collections

