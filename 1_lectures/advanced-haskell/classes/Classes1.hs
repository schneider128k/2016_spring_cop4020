data Shape = Rectangle Float Float | Circle Float deriving (Show,Eq)

area :: Shape -> Float
area (Rectangle w h) = w * h
area (Circle r)      = pi * r * r

