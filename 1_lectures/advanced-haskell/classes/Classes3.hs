-- polymorphic Shape 

-- record synatx
data Shape a =   Rectangle { width :: a, height :: a }
               | Circle    { radius :: a }
                   deriving (Show,Eq)

area :: Floating a => Shape a -> a
area (Rectangle w h) = w * h
area (Circle r)      = pi * r * r

totalArea :: Floating a => [Shape a] -> a
totalArea xs = sum (map area xs)
