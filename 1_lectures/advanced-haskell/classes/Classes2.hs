-- polymorphic Shape 

data Shape a = Rectangle a a | Circle a deriving (Show,Eq)

-- for Floating see 
-- http://hackage.haskell.org/package/base-4.8.2.0/docs/Prelude.html#t:Floating

area :: Floating a => Shape a -> a
area (Rectangle w h) = w * h
area (Circle r)      = pi * r * r

-- Note in older version of Haskell you could write
-- data (Floating a) => Shape a = ...
--
-- see http://stackoverflow.com/questions/18934882/haskell-line-of-code-not-compiling-illegal-datatype-context
