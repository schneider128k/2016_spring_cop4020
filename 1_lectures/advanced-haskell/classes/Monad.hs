-- documentation
-- https://hackage.haskell.org/package/base-4.8.2.0/docs/Control-Monad.html#t:Monad

-- source code
-- https://hackage.haskell.org/package/base-4.8.2.0/docs/src/GHC.Base.html#Monad

-- show examples in class
-- Maybe and []

-- explain relation between
-- do notation and
-- bind and lambdas

f :: Int -> [Int]
f x = [x, -x]

g :: Int -> [Int]
g y = [y, 2 * y]

ex1 :: [Int]
ex1 = return 10 >>= f >>= g

-- ****************************************************

ex2 :: [Int]
ex2 = [10] >>= (\x -> [x, -x]) >>= (\y -> [y, 2 * y])

-- rewritten using do notation

ex3 :: [Int]
ex3 = do
  x <- [10]
  y <- f x
  g y

-- ****************************************************

ex4 :: [(Int,Char)]
ex4 = do
  x <- [1,2,3]
  y <- ['a'..'b']
  [(x,y)]

-- rewritten using >>= and lambdas

ex5 :: [(Int,Char)]
ex5 = [1,2,3] >>= \x -> [(x, 'a')]

ex6 :: [(Int,Char)]
ex6 = [1,2,3]    >>= \x ->
      ['a'..'b'] >>= \y ->
      [(x,y)]

-- ****************************************************

ex7 :: [Int]
ex7 = do
  x <- [1..10]
  if even x
    then [x]
    else []

ex8 :: [Int]
ex8 =
  [1..10] >>= \x ->
  if even x
    then [x]
    else []
