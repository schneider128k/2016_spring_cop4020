borders1( belgium, france ).
borders1( france, spain ).
borders1( germany, austria ).
borders1( germany, denmark ).
borders1( germany, switzerland ).
borders1( germany, france ).
borders1( switzerland, france ).
borders1( netherlands, belgium ).
borders1( netherlands, germany ).
borders1( spain, portugal ).

borders( X, Y ) :- borders1( X, Y ).
borders( X, Y ) :- borders1( Y, X ).

% S = Start
% D = Destination

route(S, D, R) :- 
  S \= D,
  dfs( [S], D, P ),
  reverse(R, P).

dfs( [D | PrevNodes], D, [D | PrevNodes ]). 

dfs( [CurrentNode | PrevNodes], D, P) :-
  borders( CurrentNode, NewNode ),
  \+ member(NewNode, PrevNodes),
  dfs( [NewNode, CurrentNode | PrevNodes], D, P).
  
