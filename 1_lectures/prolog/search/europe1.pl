borders( belgium, france ).
borders( france, spain ).
borders( germany, austria ).
borders( germany, denmark ).
borders( germany, switzerland ).
borders( germany, france ).
borders( switzerland, france ).
borders( netherlands, belgium ).
borders( netherlands, germany ).
borders( spain, portugal ).

route( A, B ) :-
  borders( A, B ).

route( A, B ) :-
  borders( A, Z ),
  route( Z, B ).
