rev(L, R) :- acc_rev(L, [], R).

acc_rev([], A, A).
acc_rev([H|T], A, R) 
  :- acc_rev(T, [H|A], R).

% rev :: [a] -> [a]
% rev xs = rev_iter xs []

% rev_iter :: [a] -> [a] -> [a]
% rev_iter []     result  = result
% rev_iter (x:xs) partial = 
%   rev_iter xs (x:partial) 
