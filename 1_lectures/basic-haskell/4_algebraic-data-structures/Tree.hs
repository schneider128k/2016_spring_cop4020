import Data.List

data Tree a = Nil | Node a (Tree a) (Tree a)

pairEntriesWithDepth :: Tree a -> Int -> [(a, Int)]

pairEntriesWithDepth Nil _ = []
pairEntriesWithDepth (Node x left right) depth =
  (x,depth):(pairEntriesWithDepth left (depth+1) ++ pairEntriesWithDepth right (depth+1))

instance (Show a) => Show (Tree a) where
  show tree = init $ unlines [replicate d '.' ++ (show n) | (n,d) <- pairEntriesWithDepth tree 0]

instance Functor Tree where
  fmap _ Nil                 = Nil
  fmap f (Node x left right) = Node (f x) (fmap f left) (fmap f right)





  
putTree :: (Show a) => Tree a -> IO()
putTree tree = mapM_ putStrLn [replicate d '.' ++ (show n) | (n,d) <- pairEntriesWithDepth tree 0]
  

    



