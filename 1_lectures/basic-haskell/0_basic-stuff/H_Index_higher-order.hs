import Data.List

check :: (Int,Int) -> Bool
check (c,p) 
  | c >= p    = True
  | otherwise = False

--hIndex :: [Int] -> Int

hIndex xs = 
  let sorted_xs = (reverse (sort xs)) in
  length (takeWhile check (zip sorted_xs [1..]))

--  length (takeWhile check (zip sorted_xs [1..]))
--  where sorted_xs = (reverse (sort xs))

