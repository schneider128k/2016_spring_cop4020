module Fold where

foldr' :: (a -> b -> b) -> b -> [a] -> b
foldr' _ s []     = s
foldr' f s (x:xs) =
  f x (foldr' f s xs)

foldl' :: (b -> a -> b) -> b -> [a] -> b
foldl' _ s []     = s
foldl' f s (x:xs) =
  foldl' f (f s x) xs


