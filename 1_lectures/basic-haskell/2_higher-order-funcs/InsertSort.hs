ins :: (Ord a) => [a] -> a -> [a]
ins [] z = [z]
ins ys@(x:xs) z
  | z < x     = z:ys
  | otherwise = x:(ins xs z)

iSort :: (Ord a) => [a] -> [a]
iSort = foldl ins []
