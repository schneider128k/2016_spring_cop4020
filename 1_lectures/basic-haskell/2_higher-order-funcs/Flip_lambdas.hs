-- implement flip

flip' :: (a -> b -> c) -> b -> a -> c
flip' f x y = f y x

-- implement flip using a lambda function
flip'' :: (a -> b -> c) -> b -> a -> c
flip'' f = (\x y -> f y x)

-- third version
flip''' :: (a -> b -> c) -> b -> a -> c
flip''' f = anonymous
  where
    anonymous x y = f y x
