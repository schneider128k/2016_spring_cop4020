Homework 5
----------

Problem 1:
----------

Using the binary search tree in file bst.pl, implement

  contains/2
  maximum/2
  minimum/2

The following examples explain the functionality:

?- insert(nil, 2, T).
T = node(2, nil, nil).

?- insertList(nil, [2,1,3], T).
T = node(2, node(1, nil, nil), node(3, nil, nil)) .

?- contains(nil, 3).
false.

?- contains(node(3, nil, nil), 3).
true .

?- contains(node(2, node(1, nil, nil), node(3, nil, nil)), 1).
true .

?- contains(node(2, node(1, nil, nil), node(3, nil, nil)), 0).
false.

?- maximum(node(2, node(1, nil, nil), node(3, nil, nil)), X).
X = 3 .

?- minimum(node(2, node(1, nil, nil), node(3, nil, nil)), X).
X = 1 .

To test your code, you can construct many examples of binary search trees
using insertList/3 and using these trees to test contains/2, maximum/2, minimum/2.


Problem 2:
----------

Implement 

  merge/3     : merges two sorted list
  split/3     : splits a list into two lists 
  mergeSort/2 : sorts a list using the merge sort algorithm (should use merge/3 and split/3)

The following examples explain the functionality:

?- merge([1,2,4],[2,5], L).
L = [1, 2, 2, 4, 5] .

?- split([1],L1,L2).
L1 = [1],
L2 = [] .

?- split([5,1],L1,L2).
L1 = [5],
L2 = [1].

?- split([5,1,2],L1,L2).
L1 = [5, 2],
L2 = [1] .

?- split([5,1,2,3],L1,L2).
L1 = [5, 2],
L2 = [1, 3].

?- mergeSort([5,1,2,3],L).
L = [1, 2, 3, 5] .



