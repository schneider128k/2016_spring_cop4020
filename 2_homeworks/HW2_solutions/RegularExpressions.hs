module RegularExpressions where

import Prelude hiding ((<*>))

type RegExp = String -> Bool

epsilon :: RegExp
epsilon = (== "")

char :: Char ->  RegExp
char ch = (== [ch])

(|||) :: RegExp -> RegExp -> RegExp
(|||) e1 e2 =
  \s -> e1 s || e2 s

splits :: [a] -> [([a],[a])]
-- splits "fun" ~~>
-- [("","fun"),("f","un"),
--  ("fu","n"),("fun","")]
splits xs =
  map (flip splitAt xs) [0..length xs]
-- alternatively, we could also use a list comprehension like so
-- [splitAt i xs | i <- [0..length xs]]

(<*>) :: RegExp -> RegExp -> RegExp
(<*>) e1 e2 =
  \s -> or [ e1 prefix && e2 suffix | (prefix,suffix) <- splits s]

(<**>) :: RegExp -> RegExp -> RegExp
(<**>) e1 e2 =
  \s -> or [ e1 prefix && e2 suffix | (prefix,suffix) <- drop 1 (splits s)]

star :: RegExp -> RegExp
star e = epsilon ||| (e <**> star e)

-- option

option :: RegExp -> RegExp
option e = epsilon ||| e

-- plus

plus :: RegExp -> RegExp
plus e = e <*> star e

-- number

digit :: RegExp
digit = foldr1 (|||) (map char ['0'..'9'])

nonzero :: RegExp
nonzero = foldr1 (|||) (map char ['1'..'9'])

number :: RegExp
number = zero ||| ( nonzero <*> star digit )

-- fractional number

point :: RegExp
point = char '.'

zero :: RegExp
zero = char '0'

fractional :: RegExp
fractional = (zero ||| (nonzero <*> star digit)) <*> point <*> ( zero ||| ( star digit <*> nonzero  ) )
