% binary search tree

% insert

insert(nil, V, node(V, nil, nil)). 

insert(node(V, Left, Right), V, node(V, Left, Right)).

insert(node(V, Left, Right), W, node(V, LeftNew, Right)) :-
  W < V,
  insert(Left, W, LeftNew).

insert(node(V, Left, Right), W, node(V, Left, RightNew)) :-
  W > V,
  insert(Right, W, RightNew).

% insertList

insertList(T, [], T).
insertList(T1, [H | Tail], T) :-
   insert(T1, H, T2),
   insertList(T2, Tail, T).

% implement yourselft

contains(node(Val, _, _), Val).

contains(node(Val, Left, _), Key) :-
  Key < Val,
  contains(Left, Key).

contains(node(Val, _, Right), Key) :-
  Key > Val,
  contains(Right, Key).

% maximum

maximum(node(V, _, nil), V).

maximum(node(_, _, Right), V) :- 
  maximum(Right, V).

% minimum

minimum(node(V, nil, _), V).

minimum(node(_, Left, _), V) :-
  minimum(Left, V).

