merge([], L, L).

merge(L, [], L).
 
merge([H1 | T1], [H2 | T2], [H1 | T3]) :-
  H1 =< H2,
  merge(T1, [H2 | T2], T3).

merge([H1 | T1], [H2 | T2], [H2 | T3]) :-
  H1 > H2,
  merge([H1 | T1], T2, T3).

split([], [], []).

split([F], [F], []).

split([F, S | L], [F | L1], [S | L2]) :-
  split(L, L1, L2).

% mergeSort

mergeSort([],[]) :-  !.    % cut necessary  
mergeSort([H], [H]) :- !.  % cut necessary

mergeSort(L, S) :-
  split(L, L1, L2),
  mergeSort(L1, S1),
  mergeSort(L2, S2),
  merge(S1, S2, S).





