-- Modified by J. Jakes-Schauer

-- Based on Haskell - the craft of functional programming
-- section 16.7 Search tree
-- This implements a binary search tree with a minimal API
--
-- Check out http://algs4.cs.princeton.edu/32bst/ for an
-- implementation in Java with a much more extensive API

module SearchTree 
 (Tree(..),
  PreorderTree(..),
  nil,        -- Tree a
  isNil,      -- Tree a -> Bool
  isNode,     -- Tree a -> Bool
  leftSub,    -- Tree a -> Tree a
  rightSub,   -- Tree a -> Tree a
  treeVal,    -- Tree a -> a
  insTree,    -- Ord a => a -> Tree a -> Tree a
  delete,     -- Ord a => a -> Tree a -> Tree a
  minTree,     -- Ord a => Tree a -> Maybe a
  successor,
  closest
 ) where 
import Data.List (foldl')

data Tree a = Nil | Node a (Tree a) (Tree a)
  deriving (Eq) -- not deriving show because I wrote my own show function
           
-- Making a Tree officially Foldable, just for fun:
newtype PreorderTree a = Preorder (Tree a)
instance Foldable PreorderTree where
  foldr _ acc (Preorder Nil) = acc
  foldr f acc (Preorder (Node v l r)) = f v (foldr f acc' (Preorder l))
    where
      acc' = foldr f acc (Preorder r)

nil :: Tree a
nil = Nil

insTree :: Ord a => a -> Tree a -> Tree a

insTree val Nil = (Node val Nil Nil)
insTree val (Node v t1 t2)
  | v == val  = Node v t1 t2
  | (val > v) = Node v t1 (insTree val t2)
  | (val < v) = Node v (insTree val t1) t2

buildTree :: Ord a => [a] -> Tree a
buildTree = foldl' (flip insTree) Nil

delete :: Ord a => a -> Tree a -> Tree a

delete val Nil = Nil
delete val (Node v t1 t2)
  | (val > v) = Node v t1 (delete val t2)
  | (val < v) = Node v (delete val t1) t2
  | isNil t2  = t1
  | isNil t1  = t2
  | otherwise = join t1 t2

minTree :: Ord a => Tree a -> Maybe a
minTree Nil   = Nothing
minTree (Node v t1 _)
  | isNil t1  = Just v
  | otherwise = minTree t1

join :: (Ord a) => Tree a -> Tree a -> Tree a
join t1 t2 =
  Node mini t1 newt
    where
      (Just mini) = minTree t2
      newt        = delete mini t2

isNil :: Tree a -> Bool
isNil Nil = True
isNil _   = False

isNode :: Tree a -> Bool
isNode Nil          = False
isNode (Node _ _ _) = True

leftSub :: Tree a -> Tree a
leftSub Nil           = error "leftSub"
leftSub (Node _ t1 _) = t1

rightSub :: Tree a -> Tree a
rightSub Nil           = error "rightSub"
rightSub (Node _ _ t2) = t2

treeVal :: Tree a -> a
treeVal Nil          = error "treeVal"
treeVal (Node v _ _) = v

-- code to display trees

pairEntriesWithDepth :: Tree a -> Int -> [(Maybe a, Int)]

pairEntriesWithDepth Nil depth                 = [(Nothing, depth)]
pairEntriesWithDepth (Node x left right) depth =
  (Just x,depth):(pairEntriesWithDepth left (depth + 1) ++ pairEntriesWithDepth right (depth + 1))

instance (Show a) => Show (Tree a) where
  show tree = init $ unlines [replicate d '.' ++ (show' n) | (n,d) <- pairEntriesWithDepth tree 0]
    where
      show' Nothing   = "nil"
      show' (Just x)  = show x



leaf :: a -> Tree a
leaf val = Node val Nil Nil


-- Problem 1.a)

successor :: Ord a => a -> Tree a -> Maybe a
successor _ Nil = Nothing
successor x (Node v l r)
  | x >= v = successor x r
  | otherwise = Just (succ'r v l)
  where 
    -- Guarded-tail-recursive helper func:
    succ'r s Nil = s
    succ'r s (Node v l r) = if x >= v then succ'r s r else succ'r v l

-- Problem 1.b)
closest :: Int -> Tree Int -> Int

-- Helper func for `closest` (equivalent to update_closest in SearchTreeTesting.hs).  Returns whichever of a or b is metrically nearer x and defaults to the smaller if they are equidistant.
closer x a b 
  | abs (x-a) < abs (x-b) = a
  | abs (x-a) > abs (x-b) = b
  | otherwise = min a b

-- I think this is about as efficient as you can make it: guarded tail recursion with an accumulating parameter means that it should run in constant space.
-- {- 
closest _ Nil = error "Empty tree!"
closest x t@(Node v _ _) = closest' v t
  where
    closest' cur Nil = cur
    closest' cur (Node v l r)
      | v == x = x
      | otherwise = closest' (closer x v cur) (if x < v then l else r) 
--}

-- A very succinct, but not ideally efficient, alternative implementation (it always traverses the left branch, meaning it benchmarks in ms instead of μs):
closest3 x t = foldr1 (closer x) (Preorder t)

-- But here's my favorite: an efficient fold-based solution.  Because I had to write the fold functions myself, it didn't save any typing; but I like the cleanness.
closest2 x = bt_foldr1 (\v closest_l closest_r -> if x == v then x
                                                 else closer x v (if x < v then closest_l
                                                                  else closest_r))

-- Generalized right fold for a tree (could be used to implement more specific folds).  Not a "true" fold because it doesn't linearize the tree.
bt_foldr :: (a -> b -> b -> b) -> b -> Tree a -> b
bt_foldr _ acc Nil = acc
bt_foldr f acc (Node v l r) = f v l' r' where
  l' = bt_foldr f acc l
  r' = bt_foldr f acc r

-- Tree analogue of `foldr1`:
bt_foldr1 :: (a -> a -> a -> a) -> Tree a -> a
bt_foldr1 _ Nil = undefined
bt_foldr1 f t@(Node v _ _) = bt_foldr f v t




--
{-
closest = closest2
--}

--
{-
closest = closest3
--}
