import Prelude hiding (maximum)

data BST a = Nil | Node a (BST a) (BST a)
               deriving (Eq, Show)

-- a 
                        
insert :: (Ord a) => BST a -> a -> BST a

insert Nil w = Node w Nil Nil
insert (Node v left right) w
  | w == v    = Node v left            right
  | w <  v    = Node v (insert left w) right
  | otherwise = Node v left            (insert right w)

buildTree :: (Ord a) => [a] -> BST a
buildTree xs = foldr (flip insert) Nil xs

-- b

binarySearch :: (Ord a) => BST a -> a -> Bool
binarySearch Nil _ = False
binarySearch (Node v left right) w
  | w == v    = True
  | w <  v    = binarySearch left  w
  | otherwise = binarySearch right w

-- c

maximum :: BST a -> Maybe a
maximum Nil = Nothing
maximum (Node v _ Nil)   = Just v
maximum (Node _ _ right) = maximum right
