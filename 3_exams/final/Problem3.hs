-- a

filterFirstN :: (a -> Bool) -> Int -> [a] -> [a]

filterFirstN _ _ [] = []
filterFirstN _ 0 xs = xs
filterFirstN p n (x:xs)
  | not (p x) = filterFirstN p (n-1) xs
  | otherwise = x:filterFirstN p n xs

-- this could result in an infinite loop if n is negative
-- can be fixed by adding a guard | n < 0 = error "negative n"

-- b

insert :: Ord a => a -> [a] -> [a]

insert x []     = [x]
insert x zs@(y:ys)
  | x <= y    = x :zs
  | otherwise = y:insert x ys

-- c

insertSort :: Ord a => [a] -> [a]
insertSort = foldr insert []


