-- implement a binary tree that holds
-- elements of type a

-- implement a function sum that takes a
-- tree and sums up all the elements
-- what class contraint do you need?

data Tree a =
    Nil
  | Node a (Tree a) (Tree a)
    deriving (Show,Eq)

--instance Show a => Show (Tree a) where
--  show Nil = 

sumTree :: Num a => Tree a -> a
sumTree Nil = 0
sumTree (Node n left right) =
  n + (sumTree left) + (sumTree right)

--occurs :: Eq a =>  a -> Tree a -> Bool

-- Show, Eq, Ord
