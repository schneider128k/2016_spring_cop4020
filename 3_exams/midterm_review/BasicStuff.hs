import Prelude hiding (map,filter)

-- reimplement filter and map using primitive recursion

map :: (a -> b) -> [a] -> [b]
map _ []     = []
map f (x:xs) = f x:(map f xs)

filter :: (a -> Bool) -> [a] -> [a]
filter _ []     = []
filter p (x:xs) =
  if p x
    then x:(filter p xs)
    else   (filter p xs)

-- do it with guard instead of if 

filterFirst :: (a -> Bool) -> [a] -> [a]
filterFirst _ [] = []
filterFirst p (x:xs)
  | p x       = x:(filterFirst p xs)
  | otherwise = xs

-- partial application
-- (.) function composition

filterLast :: (a -> Bool) -> [a] -> [a]
filterLast p =
  reverse . (filterFirst p) . reverse

-- reimplement using list comprehensions

map' :: (a -> b) -> [a] -> [b]
map' f xs = [f x | x <- xs] 

filter' :: (a -> Bool) -> [a] -> [a]
filter' p xs = [x | x <- xs, p x]

-- produce all pair (a,b) where a from list
-- xs abd b from list ys


